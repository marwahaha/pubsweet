# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.0.0"></a>
# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@0.2.2...@pubsweet/default-theme@1.0.0) (2018-05-03)


### Bug Fixes

* **theme:** disable unused theme variable ([91691ff](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/91691ff))
* **theme:** remove warning color ([c0897c8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c0897c8))
* **theme:** simplify transitions ([90c72ff](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/90c72ff))


### BREAKING CHANGES

* **theme:** might break components that used the boxShadow variable
* **theme:** transitions might not work for components that used the Xs, S and M values
* **theme:** might break components that used the warning colors




<a name="0.2.2"></a>
## [0.2.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@0.2.1...@pubsweet/default-theme@0.2.2) (2018-04-11)




**Note:** Version bump only for package @pubsweet/default-theme

<a name="0.2.1"></a>
## [0.2.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@0.2.0...@pubsweet/default-theme@0.2.1) (2018-03-15)




**Note:** Version bump only for package @pubsweet/default-theme

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@0.1.0...@pubsweet/default-theme@0.2.0) (2018-03-05)


### Features

* **default-theme:** add variables to default theme ([ba121b0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ba121b0))
* **normalize:** add normalize css ([9eb24e5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9eb24e5))
* **ui:** add theming to Tags ([ee959d2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ee959d2))
* **ui:** add theming to ValidatedField ([c2a1d54](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c2a1d54))




<a name="0.1.0"></a>

# 0.1.0 (2018-02-08)

### Features

* **theme:** add default theme package ([5231a56](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5231a56))
